import {
  chose,
  currentGlass,
  renderListGlass,
} from "./controller/tryGlassController.js";

document.querySelector("#vglassesList").innerHTML = renderListGlass();

window.chose = chose;

const removeGlasses = (bool) => {
  const e = document.querySelector("#glassTry");
  bool ? (e.style.display = "block") : (e.style.display = "none");
};

window.removeGlasses = removeGlasses;
